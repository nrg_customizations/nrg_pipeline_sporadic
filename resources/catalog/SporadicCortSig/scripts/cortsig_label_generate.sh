#!/bin/bash

VERSION=1

die(){
    echo >&2 "$@"
    exit 1
}


# Give pipeline engine the version when it asks for it
if [ "${1}" == "-version" ] ||  [ "${1}" == "--version" ] || [ "${1}" == "-v" ] ; then
    echo $VERSION
    exit 0
fi

# Get input args
FS_VERSION=$1
SUBJECTS_DIR=$2
SESSION_ID=$3

export SUBJECTS_DIR=$SUBJECTS_DIR

SCRIPTS_HOME="@PIPELINE_DIR_PATH@/scripts"

# Determine FS setup script from FS version variable
case $FS_VERSION in
    5.0)
    die "This pipeline can't be run on FreeSurfer version $FS_VERSION"
    ;;
    5.1)
    die "This pipeline can't be run on FreeSurfer version $FS_VERSION"
    ;;
    5.3-HCP)
    SETUP="$SCRIPTS_HOME/freesurfer53-HCP_setup.sh"
    ;;
    5.3-HCP-patch)
    SETUP="$SCRIPTS_HOME/freesurfer53-patch_setup.sh"
    ;;
    6.0)
    die "This pipeline can't be run on FreeSurfer version $FS_VERSION"
    ;;
    *)
    die "I don't recognize FreeSurfer version $FS_VERSION"
    ;;
esac

# Set up Freesurfer
source $SETUP || die "Could not source setup script $SETUP"

# run mri_label2label for left and right hemispheres
mri_label2label --srclabel $SUBJECTS_DIR/fsaverage/label/lh.SporadicCortSig.label --srcsubject fsaverage --trglabel $SUBJECTS_DIR/$SESSION_ID/label/lh.SporadicCortSig.label --trgsubject $SESSION_ID --regmethod surface --hemi lh

mri_label2label --srclabel $SUBJECTS_DIR/fsaverage/label/rh.SporadicCortSig.label --srcsubject fsaverage --trglabel $SUBJECTS_DIR/$SESSION_ID/label/rh.SporadicCortSig.label --trgsubject $SESSION_ID --regmethod surface --hemi rh

# run mris_anatomical_label for left and right hemispheres
mris_anatomical_stats -l $SUBJECTS_DIR/$SESSION_ID/label/lh.SporadicCortSig.label -f $SESSION_ID.lh.SporadicCortSig.csv $SESSION_ID lh

mris_anatomical_stats -l $SUBJECTS_DIR/$SESSION_ID/label/rh.SporadicCortSig.label -f $SESSION_ID.rh.SporadicCortSig.csv $SESSION_ID rh