#!/bin/env python
#
# Run this from the Freesurfer SUBJECTS_DIR
# subject_id must be a valid Freesurfer subject
import sys, json, argparse, csv


parser = argparse.ArgumentParser(description='Pull labels from freesurfer aparc and aseg files')
parser.add_argument('input_filename', help="Input filename of a stats output file")
parser.add_argument('output_filename', help="Output filename for resulting file")

args=parser.parse_args()

input_filename = args.input_filename
output_filename = args.output_filename

all_rows = []

headers = []
column_headers = []


def open_one_file(filename):


	# Go through the file and extract the important information
	with open(filename) as f:
		data = f.readlines()

		column_headers_arr = []
		columns_dict = {}

		one_row_dict = {}
		
		# Go through and get the hemisphere first so we can use it in the column names
		hemisphere = "" 
		
		for single_line in data:
			if single_line.startswith("# hemi"):
				hemi_line_remainder = single_line.split("# hemi ")
				hemisphere = hemi_line_remainder[1].split(",")[0].strip('\n').strip('"')

		# Get the measure info and the column headers at the end of the file
		for single_line in data:
			
			if single_line.startswith("# ColHeaders "):
				# line looks like this:
				# # ColHeaders StructName NumVert SurfArea GrayVol ThickAvg ThickStd MeanCurv GausCurv FoldInd CurvInd,,,,
				colheaders_line_remainder = single_line.split("# ColHeaders ")
				column_headers_arr = colheaders_line_remainder[1].split()
				map(str.strip, column_headers_arr)

				# Strip trailing commas
				column_headers_arr[-1] = column_headers_arr[-1].strip(",")

				for i in column_headers_arr:
					headers.append(hemisphere + "_" + i.strip('\n').strip('"'))
					column_headers.append(hemisphere + "_" + i.strip('\n').strip('"'))

		# Go through one more time and get the resulting values that go with the column headers
		# Do this in a separate pass just in case
		for single_line in data:		
			if not single_line.startswith("#"):
				first_line_contents_arr = single_line.split(".label")

				label_name = first_line_contents_arr[0]

				line_contents_arr = [label_name] + first_line_contents_arr[1].split()

				map(str.strip, line_contents_arr)

				# Strip trailing commas
				line_contents_arr[-1] = line_contents_arr[-1].strip(",")

				if len(line_contents_arr) == len(column_headers):
					for index, val in enumerate(column_headers):
						one_row_dict[val] = line_contents_arr[index].strip()


		all_rows.append(one_row_dict)

open_one_file(input_filename)

with open(output_filename, 'w') as outfile:
    w = csv.DictWriter(outfile, fieldnames=headers)
    w.writeheader()
    for single_dict in all_rows:
    	w.writerow(single_dict)