#!/bin/env python
import os, sys, csv, argparse
from lxml import etree

versionNumber='1'
dateString='20190117'
author='sarahkeefe'
progName=os.path.basename(sys.argv[0])
idstring = '$Id: %s,v %s %s %s $'%(progName,versionNumber,dateString,author)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# Parse input args
parser = argparse.ArgumentParser(description='Write Sporadic AD Cortical Signature stats to FreeSurfer assessor XML.')
parser.add_argument('-v', '--version',
                    help='Print version number and exit',
                    action='version',
                    version=versionNumber)
parser.add_argument('--idstring',
                    help='Print id string and exit',
                    action='version',
                    version=idstring)
parser.add_argument('freesurfer_xml',help="Path to existing FreeSurfer assessor XML.")
parser.add_argument('cortsig_stats_file',help='The Sporadic AD Cortical Signature stats file')
parser.add_argument('output_filename',help='Output ILP assessor XML.')
parser.add_argument('--debug',action='store_true')
args=parser.parse_args()

freesurfer_xml = args.freesurfer_xml
cortsig_stats_file = args.cortsig_stats_file
output_filename = args.output_filename

# Read csvs. Build some sort of object with them
print "Reading Sporadic AD Cortical Signature stats file: " + cortsig_stats_file
with open(cortsig_stats_file) as f:
    cortsig_stats_results = [row for row in csv.reader(f)]
print "Done\n"

# Read FreeSurfer XML
print "Reading Freesurfer assessor XML: " + freesurfer_xml
fsxml = etree.parse(freesurfer_xml)
print "Done\n"
fsroot = fsxml.getroot()
fsns = fsroot.nsmap["fs"]

print "Adding SporadicCortSig stats to Freesurfer assessor..."
for cortSigResultHeader,stat in zip(*cortsig_stats_results):
    if args.debug: print "SporadicCortSig ROI '%s'"%cortSigResultHeader

    if stat == 'NA':
        if args.debug:
            print "Replacing NA with NaN."
        stat = 'NaN'
    elif not (stat == 'Inf' or stat == '-Inf' or stat == 'NaN' or not is_number(stat)):
        #if args.debug:
            #print "Truncating stat decimal places. {0}->{0:.3f}".format(float(stat))
        stat = str(float(stat))

    summaryMeasure = cortSigResultHeader.startswith("summary")
    session = cortSigResultHeader.startswith("session")

    leftRight = ""

    if summaryMeasure:
        if args.debug: print "CortSig Summary measure is '%s'"%stat

    if cortSigResultHeader.startswith("lh_"):
        leftRight = "left"
        leftRightName = "Left"
        if args.debug and leftRight: print "CortSig ROI '%s' is a %s stat"%(cortSigResultHeader,leftRight)
        cortSigResultHeader = cortSigResultHeader.split("_", 1)[1]

    elif cortSigResultHeader.startswith("rh_"):
        leftRight = "right"
        leftRightName = "Right"
        if args.debug and leftRight: print "CortSig ROI '%s' is a %s stat"%(cortSigResultHeader,leftRight)
        cortSigResultHeader = cortSigResultHeader.split("_", 1)[1]

    else:
        leftRight = ""

    # Add the CortSig stat
    if not summaryMeasure and not session and not cortSigResultHeader.endswith("StructName"):

        # First check if we have a sporadicCortSig section in the XML
        existingCortSigHemisphereRoiSection = fsroot.xpath("fs:measures/fs:surface/fs:hemisphere[@name='%s']/fs:regions/fs:region[@name='sporadiccortsig']"%leftRight, namespaces=fsroot.nsmap)
        # If it doesn't exist, create it
        if not existingCortSigHemisphereRoiSection:
            # Get the element to use as the parent
            existingCortSigHemisphereNode = fsroot.xpath("fs:measures/fs:surface/fs:hemisphere[@name='%s']/fs:regions"%leftRight, namespaces=fsroot.nsmap)[0]
            # Add this region to the hemisphere/regions node if we don't have it yet
            print "Adding section region name=sporadicCortSig to FS measures"
            cortSigHemisphereRoiSectionNode = etree.SubElement(existingCortSigHemisphereNode, "{%s}region"% fsns, {"name":"sporadiccortsig"})
        else:
            print "We've already got an XML element for surface/hemisphere/regions/region=sporadiccortsig. Don't need to add one."
            cortSigHemisphereRoiSectionNode = existingCortSigHemisphereRoiSection[0]

            # remove extra region nodes if we have them
            if len(existingCortSigHemisphereRoiSection) > 1:
                for extraCortSigRegionEl in existingCortSigHemisphereRoiSection[1:]:
                    existingCortSigHemisphereRoiSection.remove(extraCortSigRegionEl)
                    if args.debug: print "Removing superfluous 'region=sporadiccortsig' element"

        # Check for the individual elements that go in the region section
        # Check and see if we have this element in surface/hemisphere/regions/region yet
        existingCortSigHemisphereRegionRoiElList = fsroot.xpath('fs:measures/fs:surface/fs:hemisphere[@name="%s"]/fs:regions/fs:region[@name="sporadiccortsig"]/fs:%s' % (leftRight,cortSigResultHeader), namespaces=fsroot.nsmap)

        # If we have the hemisphere/region/element, overwrite it with the new stat
        if existingCortSigHemisphereRegionRoiElList:
            existingCortSigHemisphereRegionRoiEl = existingCortSigHemisphereRegionRoiElList[0]
            oldstat = existingCortSigHemisphereRegionRoiEl.text
            existingCortSigHemisphereRegionRoiEl.text = stat
            print "Overwriting existing stat {0}={1} with new stat {0}={2} for FS cortical signature ROI '{3} {0}'".format(cortSigResultHeader,oldstat,stat,leftRight)

            # If we have extra nodes that match this name, delete the rest
            if len(existingCortSigHemisphereRegionRoiElList) > 1:
                for extraCortSig in existingCortSigHemisphereRegionRoiElList[1:]:
                    existingCortSigHemisphereRegionRoiEl.remove(extraCortSig)
                    if args.debug: print "Removing superfluous '%s' element"%cortSigResultHeader
        else:
            # If we don't have the node yet, add it at surface/hemisphere/regions/region/element. 
            existingHemisphereRegionNodeList = fsroot.xpath('fs:measures/fs:surface/fs:hemisphere[@name="%s"]/fs:regions/fs:region[@name="sporadiccortsig"]' % leftRight, namespaces=fsroot.nsmap)
            existingHemisphereRegionNode = existingHemisphereRegionNodeList[0]

            etree.SubElement(existingHemisphereRegionNode, "{%s}%s"% (fsns,cortSigResultHeader)).text = stat
            print "Adding stat %s=%s to FS cortical signature ROI '%s %s'"%(cortSigResultHeader,stat,leftRight,cortSigResultHeader)


    elif not session and not cortSigResultHeader.endswith("StructName"):
        # Deal with the summary measure
        # Put it in the main section of the Freesurfer measures
        existingCortSigSummaryElList = fsroot.xpath('fs:sporadiccortsig_summary', namespaces=fsroot.nsmap)
        if existingCortSigSummaryElList:
            oldstat = existingCortSigSummaryElList[0].text
            existingCortSigSummaryElList[0].text = stat
            print "Overwriting existing stat sporadiccortsig_summary={0} with new stat sporadiccortsig_summary={1} for FS cortical signature ROI".format(oldstat,stat)

            if len(existingCortSigSummaryElList) > 1:
                for extraCortSig in existingCortSigSummaryElList[1:]:
                    existingCortSigSummaryElList.remove(extraCortSig)
                    if args.debug: print "Removing superfluous 'sporadiccortsig_summary' element"
        else:
            # add it to the Freesurfer root element
            etree.SubElement(fsroot, "{%s}sporadiccortsig_summary"%fsns).text = stat
            print "Adding stat summary=%s to FS cortical signature ROI 'sporadiccortsig_summary'"%stat        
    print
print "Done\n"

# Write out finished ILP assessor
print 'Writing assessor XML to %s'%output_filename
with open(output_filename,'w') as f:
    f.write(etree.tostring(fsxml, encoding='UTF-8', xml_declaration=True, pretty_print=True))
print "Done"
